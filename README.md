# CRUD

### Create DB Name
```MySQL
create database dbname;
```

### https://start.spring.io
> Spring Data JPA
> MySQL Driver
> Spring Web 


### Edit application.properties
Added

```
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://localhost:3306/dbname?zeroDateTimeBehavior=convertToNull
spring.datasource.username=tuUsuarioChingon
spring.datasource.password=TuContrasena
```

# Logic Program Java

1. Create Class as Entity(Table)
2. Create Repository Interface
3. Create RESTController

## Execute Proyect


