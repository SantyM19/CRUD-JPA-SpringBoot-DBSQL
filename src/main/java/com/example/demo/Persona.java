package com.example.demo;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity //Esto le dice a Hibernate que cree una tabla basada en la clase.
public class Persona {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer idPersona;

    private String nombre;

    private String paterno;

    public Persona(Integer idPersona, String nombre, String paterno) {
        this.idPersona = idPersona;
        this.nombre = nombre;
        this.paterno = paterno;
    }

    public Persona() {
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }
}